import React from 'react';

// AppNavigator
import AppNavigator from '@navigations/appNavigator';

const App = () => {
  return <AppNavigator />;
};

export default App;
