import React, {useContext} from 'react';

// Components
import {AuthContext} from '@contexts/context';
import en from '@components/configs/en';
import mm from '@components/configs/mm';

export const useLocal = () => {
  const {lang} = useContext(AuthContext);
  if (lang === 'en') {
    return en;
  } else {
    return mm;
  }
};
