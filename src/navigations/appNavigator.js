import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import {AuthContext} from '@contexts/context';
import {NavigationContainer} from '@react-navigation/native';

// Stack
import AuthStack from './stack/authStack';

const appNavigator = () => {
  const [splashScreen, setSplashScreen] = useState(true);
  // const [auth, setAuth] = useState(false);

  useEffect(() => {
    storeData();
  }, []);

  const storeData = () => {
    try {
        setSplashScreen(false);
    } catch (error) {
      console.log('error', error);
    }
  };

  if (splashScreen) {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Welcome to our app</Text>
      </View>
    );
  } else {
    return (
      <AuthContext.Provider value="Test">
        <NavigationContainer>
          <AuthStack />
        </NavigationContainer>
      </AuthContext.Provider>
    );
  }
};

export default appNavigator;
