import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Screens
import LoginScreen from '@screens/auth/Login/Login';

const Stack = createNativeStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={LoginScreen} />
    </Stack.Navigator>
  );
};

export default AuthStack;
