import React, {useState, useContext} from 'react';
import {View, Text, ToastAndroid, TouchableOpacity} from 'react-native';

// Components
import Header from '@components/login/header';
import styles from './Style';

// Hooks
import { useLocal } from '@hooks';

const Login = ({navigation}) => {
  const local = useLocal();
  return (
    <View>
      <View style={styles.languageContainer}>
        <TouchableOpacity onPress={() => changeLanguage('en')}>
          <Text>English</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => changeLanguage('mm')}>
          <Text>Myanmar</Text>
        </TouchableOpacity>
      </View>
      <Header
        title={local.login}  
        buttonText={local.login}
        footerAction={footerHandler}
      />
    </View>
  );
};

export default Login;
