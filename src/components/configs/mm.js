export default {
  login: 'အကောင့်ဝင်ရန်',
  register: 'အကောင့်ဖွင့်ရန်',
  userName: 'အမည်....',
  emailPlaceholder: 'အီးမေးလ်....',
  pwdPlaceholder: 'စကားဝှက်....',
  noAccount: 'အကောင့်မရှိပါက ',
  already: 'အကောင့်ရှိလျှင်',
};
